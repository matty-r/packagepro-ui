export let repoUrl = 'https://gitlab.com/matty-r/packagepro-ui';
export let appName = 'Arch Package Pro';

export function getFileName(path: string): string {
  let lines = path.split('/');
  let out: string = lines[0];

  for (let i = 1; i < lines.length; i++) {
    let tabs = new Array(i + 1).join('  ');
    out += '\n' + tabs + '└─ ' + lines[i];
  }

  return out;
}

export function humanFileSize(bytes: number, si: boolean): string {
  let thresh = si ? 1000 : 1024;
  if (Math.abs(bytes) < thresh) {
    return `${bytes} B`;
  }
  let units = si
    ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
    : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
  let u = -1;
  do {
    bytes /= thresh;
    ++u;
  } while (Math.abs(bytes) >= thresh && u < units.length - 1);
  return `${bytes.toFixed(1)} ${units[u]}`;
}
