import { Component } from 'inferno';
import { repoUrl } from '../utils';
import { appName } from '../utils';

export class Home extends Component<any, any> {
  render() {
    return <div class="container">{this.onboard()}</div>;
  }

  onboard() {
    return (
      <p class="text-justify">
        <a href={repoUrl}>{appName}</a> is a <i>collaborative</i> git repository
        of packages, consisting of a single, searchable <code>packages</code>{' '}
        database. New packages are periodically added from various mirrors. This
        comes with a self-hostable webserver, a command line search, and a
        folder scanner to add packages.
        <br />
        <br />
        <a href={repoUrl}>{appName}</a> will only store packages with at least
        one mirror to keep the file small, will be periodically purged of
        non-mirrored packages, and sorted by mirrors descending.
        <br />
        <br />
        API: Not currently used <br />
        <br />
        To request more packages, or add your own, go <a href={repoUrl}>here</a>
        .<br />
        <br />
        Made with <a href="https://www.rust-lang.org">Rust</a>,{' '}
        <a href="https://actix.rs/">Actix</a>,{' '}
        <a href="https://www.infernojs.org">Inferno</a>, and{' '}
        <a href="https://www.typescriptlang.org/">Typescript</a>.
      </p>
    );
  }
}
