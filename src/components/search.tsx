import { Component, linkEvent } from 'inferno';
import moment from 'moment';

import { endpoint } from '../env';
import { SearchParams } from '../interfaces';
import { humanFileSize, getFileName, repoUrl } from '../utils';

interface State {
  searchParams: SearchParams;
  searching: boolean;
}

export class Search extends Component<any, State> {
  state: State = {
    searchParams: {
      q: '',
      page: 1,
      type_: 'core',
    },
    searching: false,
  };

  constructor(props: any, context: any) {
    super(props, context);
  }

  componentDidMount() {
    this.state.searchParams = {
      page: Number(this.props.match.params.page),
      q: this.props.match.params.q,
      type_: this.props.match.params.type_,
    };
  }

  // Re-do search if the props have changed
  componentDidUpdate(lastProps: any) {
    if (lastProps.match && lastProps.match.params !== this.props.match.params) {
      this.state.searchParams = {
        page: Number(this.props.match.params.page),
        q: this.props.match.params.q,
        type_: this.props.match.params.type_,
      };
    }
  }

  async fetchData(searchParams: SearchParams): Promise<[]> {
    let q = encodeURI(searchParams.q);
    return (
      await fetch(
        `${endpoint}/service/search?q=${q}&page=${searchParams.page}&type_=${searchParams.type_}`
      )
    ).json();
  }

  render() {
    return (
      <div>{this.state.searching ? this.spinner() : this.noResults()}</div>
    );
  }

  spinner() {
    return (
      <div class="text-center m-5 p-5">
        <svg class="icon icon-spinner spinner">
          <use xlinkHref="#icon-spinner"></use>
        </svg>
      </div>
    );
  }

  noResults() {
    return (
      <div class="text-center m-5 p-5">
        <h1>No Results</h1>
      </div>
    );
  }

  paginator() {
    return (
      <nav>
        <ul class="pagination justify-content-center">
          <li
            className={
              this.state.searchParams.page == 1
                ? 'page-item disabled'
                : 'page-item'
            }
          >
            <button
              class="page-link"
              onClick={linkEvent({ i: this, nextPage: false }, this.switchPage)}
            >
              Previous
            </button>
          </li>
          <li class="page-item">
            <button
              class="page-link"
              onClick={linkEvent({ i: this, nextPage: true }, this.switchPage)}
            >
              Next
            </button>
          </li>
        </ul>
      </nav>
    );
  }

  switchPage(a: { i: Search; nextPage: boolean }) {
    let newSearch = a.i.state.searchParams;
    newSearch.page += a.nextPage ? 1 : -1;
    a.i.props.history.push(
      `/search/${newSearch.type_}/${newSearch.q}/${newSearch.page}`
    );
  }

  copyLink(evt: any) {
    const href = evt.currentTarget.dataset.href;
    try {
      navigator.clipboard
        .writeText(href)
        .then(() => alert('Copied magnet URL to clipboard'));
    } catch {
      alert(`Could not copy magnet URL: ${href}`);
    }
  }
}
