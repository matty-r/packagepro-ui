import { Component, linkEvent } from 'inferno';

interface ModalState {
  isOpen: boolean;
}

export class MyModal extends Component {
  state: ModalState = {
    isOpen: false,
  };

  render() {
    if (this.state.isOpen) {
      return this.myModal();
    } else {
      return null;
    }
  }

  constructor(props) {
    super(props);
    this.state.isOpen = false;

    this.toggleModal = this.toggleModal.bind(this);
  }

  toggleModal = () => {
    console.log('toggleModal - isOpen?');
    console.log(this.state.isOpen);
    this.setState({
      isOpen: !this.state.isOpen,
    });
  };

  myModal() {
    return (
      <div class="modal">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Modal title</h5>
              <button
                type="button"
                class="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Modal body text goes here.</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary">
                Save changes
              </button>
              <button
                type="button"
                class="btn btn-secondary"
                data-dismiss="modal"
              >
                Close
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
