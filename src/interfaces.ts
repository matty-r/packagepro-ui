export interface SearchParams {
  q: string;
  page: number;
  size?: number;
  type_: string;
}
