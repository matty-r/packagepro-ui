# A simple UI for PackagePro written in Inferno

#### use node v16.13.0 (lts version)

first run for dependencies

```
npm install
```

then run to build dir

```
npm run build
```

or run for dev, connects to localhost:4444

```
npm run start
```

